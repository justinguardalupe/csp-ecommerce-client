import React from 'react';
import { useState, useEffect } from 'react';
import { Container, Card, Button, Row, Col, Span, Form } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import ProductCard from '../components/ProductCard';

export default function ProductView(props) {
	console.log(props);
	console.log(props.match.params.productId);
	const productId = props.match.params.productId;
	const [name, setName] = useState([]);
	const [image, setImage] = useState([]);
	const [description, setDescription] = useState([]);
	const [price, setPrice] = useState([]);
	const userId = localStorage.getItem('id');

	useEffect(() => {
		fetch(`https://fathomless-forest-22794.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setImage(data.image);
			setDescription(data.description);
			setPrice(data.price);
		}
	);
	
}, [])

	const addToCart = async () => {
	        fetch(`https://fathomless-forest-22794.herokuapp.com/carts/${userId}`, {
	            method: 'PUT',
	            headers: {
	                'Content-Type': 'application/json'
	            },
	            body: JSON.stringify({
	                customerId: userId,
	                name : name,
	                image : image,
	                description : description,
					productId: productId,
					price: price,
	            })
	        })
	        .then(res => res.json())
	        .then(data => {
	            console.log(data);
	   	    })
		}


return(
	
	<Container className="mt-5" style={{width:700}}>
		<Card>
			<Card.Body>
				<Card.Header as="h6">
					<Form.Label className="mr-3">
					</Form.Label>
					<Form.Label className="productDetails">
						{name}
					</Form.Label>
				</Card.Header>
				<Card.Subtitle as="h5" className="mt-2 ml-3">
					<div className="d-flex justify-content-center">
					<Card.Img className="mt-2 ml-3" src={image} style={{width:280}}/>
					</div>
						<hr></hr>
					<Form.Label className="productDetails">
						{description}
					</Form.Label>
				</Card.Subtitle>
				<Card.Subtitle as="h5" className="mt-2 ml-3">
					<Form.Label className="mr-3">
					Price:
					</Form.Label>
					<Form.Label className="productDetails">
						{price}
					</Form.Label>
				</Card.Subtitle>
				<hr></hr>
				<div className="d-flex justify-content-center">
			<Link className="btn btn-primary" to={`carts/${userId}`} variant="primary" size="lg" style={{width:320}} onClick={addToCart} block>Add to Cart</Link>
			</div>
			</Card.Body>
		</Card>
	</Container>
	)
};
