import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button, Row, Col } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(props){
    // Allows us to consume the User context object and it's properties to use for user validation 
    const {user, setUser} = useContext(UserContext);
	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password);

	// Function to simulate user registration
	function authenticate(e){
		e.preventDefault();
		// Fetch request to process the backend API
		// Syntax: fetch('url', {options})
		// .then(res => res.json())
		// .then(data => {})
		fetch('https://fathomless-forest-22794.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
		// 	// If no user information is found, the "access" property will not be available and will return undefined
		if(typeof data.access !== "undefined"){
		 		// The token will be used to retrieve user information across the whole frontend application and storing it in the localStorage to allow ease of access to the user's information
		 		localStorage.setItem('token', data.access);

		 		retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "You may now view our products!"
				})
			}
			else{
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again."
				})
			}
		})

		// Set the email of the authenticated user in the local storage
		// Syntax: localStorage.setItem('propertyName', value);
		/*localStorage.setItem('email', email);*/

		// Set the global user state to have properties obtained from our local storage
		/*setUser({
			email: localStorage.getItem('email')
		});*/

		setEmail('');
		setPassword('');

			//alert('You are now logged in.')
	}

	const retrieveUserDetails = (token) => {
		fetch('https://fathomless-forest-22794.herokuapp.com/users/details', {
			headers: {
				Authorization: `Bearer ${ token }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		// Validate to enable submit button when all fields are populated and both passwords match 
		if((email !== '' && password !==''))
		{
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password])
	

	localStorage.setItem('id', user.id);
	localStorage.setItem('isAdmin', user.isAdmin);

		return(
			(user.id !== null) ?
			(user.isAdmin === true) ?

				<Redirect to="/admin" />
				:
				<Redirect to="/products" />
			:
			<Container className="auth-page">
			        <Row className="d-flex align-items-center">
			            <Col xs={12} lg={6} className="left-column">
			                <div className="widget-auth">
			                    <div className="d-flex align-items-center justify-content-between py-3">
			                        <p className="auth-header mb-0">Sign In</p>
			                    </div>

			                <Form onSubmit={(e) => authenticate(e)}>
			                    <Form.Group controlId="userEmail">
			                        <Form.Text>Email address</Form.Text>
			                        <Form.Control 
			                            type="email" 
			                            placeholder="Enter email"
			                            className="input-transparent pl-3" 
			                            value={email}
			                            onChange={(e) => setEmail(e.target.value)}
			                            required
			                        />
			                    </Form.Group>

			                    <Form.Group controlId="password" className="mt-3">
			                        <Form.Text>Password</Form.Text>
			                        <Form.Control 
			                            type="password" 
			                            placeholder="Password" 
			                            className="input-transparent pl-3"
			                            value={password}
			                            onChange={(e) => setPassword(e.target.value)}
			                            required
			                        />
			                    </Form.Group>
			                      	{ isActive ? 
			                      		<div className="bg-widget d-flex justify-content-center">
			                      			<Button className="my-3" variant="outline-success" type="submit" id="submitBtn">
			                        			<b>Submit</b>
			                      			</Button>
			                      		</div>
			                      		: 
			                      		<div className="bg-widget d-flex justify-content-center">
			                      			<Button className="my-3" type="submit" id="submitBtn" variant="outline-dark" disabled>
			                        			<b>Submit</b>
			                      			</Button>
			                      		</div>
			                    	}
			                </Form>
			                </div>
			            </Col>
			        </Row>
			    </Container>
			    )
			}
