import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){
	const data={
		title: "BURGLER'S",
		content: "Highest quality burgers now within reach!",
		destination: "/products",
		label: "Order Now!"
	}

	return(
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}
