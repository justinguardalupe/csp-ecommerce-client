import { Table, Container, Row, } from 'react-bootstrap';
import { Fragment, useEffect, useState, useContext } from 'react';
import AllOrderTable from '../components/AllOrderTable';
import UserContext from '../UserContext';


export default function OrderHistory(){
	const [orders, setOrders] = useState ([]);


	useEffect(() => {
		fetch('https://fathomless-forest-22794.herokuapp.com/orders/users/orders')
			
		.then(res => res.json())
		.then(data => {
			console.log(data);

// Sets the "products" state to map the data retrieved from the fetch request in several "ProductCard Components"
			setOrders(data.map(order => {
					return (
						<AllOrderTable key={order.id} orderProp={order} />
					)
				})
			);		
		})
	}, [])
		

return (
			
		<Container>
		<Fragment> 
		<h1 className="mt-3 mb-4">All Orders</h1>
		<Table>
					<thead>
					    <tr>
					      <th><div className="d-flex justify-content-center">Order ID</div></th>
					      <th><div className="d-flex justify-content-center">Purchase Date</div></th>
					      <th><div className="d-flex justify-content-center">Item/s</div></th>
					      <th><div className="d-flex justify-content-center">Total</div></th>
					    </tr>
					  </thead>
		</Table>
		</Fragment>
		<Row>

		<Fragment> 
			{orders}
		</Fragment>
		</Row>
		</Container>
	)
}




