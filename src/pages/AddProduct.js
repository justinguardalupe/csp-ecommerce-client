import { useState, useEffect, useContext } from 'react';
import React from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
//import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Container } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

export default function AddProduct(){

	const {user, setUser} = useContext(UserContext);
	// State hooks to store the values of the input fields
	const [image, setImage] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	
	let history = useHistory();

	// Function to simulate adding product
	async function AddProduct(e){
		  e.preventDefault();
		  fetch('https://fathomless-forest-22794.herokuapp.com/products/create',{
		    method: 'POST',
		    headers:{
		        'Content-Type':'application/json'
		    },
		    body: JSON.stringify({
		        name:name,
		        image:image,
		        description:description,
		        price:price
		    })
		  })
		  .then(res => res.json())
		  .then(data => {
						console.log(`${data}dataDATA`);
		        Swal.fire({
		            title:"Product added",
		            icon:"success",
		            text:"Check the database to view full list of products."
		        });
		        history.push('/admin')
		      })
}

return (	
		<Container>
		<h2 className="mt-4 mb-1">New Product</h2>
		<Form onSubmit={(e) => AddProduct(e)}>
				<Form.Group controlId="productName">
			    <Form.Label className="mt-4 mb-1">Product Name</Form.Label>
			    <Row>
				<Col xs={4}>
			    <Form.Control size="sm"
			     	type="productname" 
			     	placeholder="" 
			     	value={name}
			     	onChange={e => setName(e.target.value)}
			     	required />
			    </Col>
			    </Row>
			  	</Form.Group>


			  	<Form.Group controlId="productName">
			    <Form.Label className="mt-4 mb-1">Product Image</Form.Label>
			    <Row>
				<Col xs={4}>
			    <Form.Control size="sm"
			     	type="productimage" 
			     	placeholder="" 
			     	value={image}
			     	onChange={e => setImage(e.target.value)}
			     	required />
			    </Col>
			    </Row>
			  	</Form.Group>


			  	<Form.Group controlId="productDescription">
			    <Form.Label className="mt-4 mb-1">Description</Form.Label>
			    <Row>
				<Col xs={4}>
			    <Form.Control size="sm"
			     	type="productdescription" 
			     	placeholder="" 
			     	value={description}
			     	onChange={e => setDescription(e.target.value)}
			     	required />
			    </Col>
			    </Row>
			  	</Form.Group>
			  	<Form.Group controlId="productPrice">
			    <Form.Label className="mt-4 mb-1">Price</Form.Label>
			    <Row>
				<Col xs={4}>
			    <Form.Control size="sm" 
			    	type="productprice" 
			    	placeholder="" 
			    	value = {price}
			    	onChange = { e => setPrice(e.target.value)}
			    	required />
			    </Col>
			    </Row>
			  	</Form.Group>
					<Button className="mt-4 mb-1" variant="outline-success" type="submit" id="submitBtn">
			 		   Add Product
			 		</Button>
				
				  
		</Form>
		</Container>
	)
}
