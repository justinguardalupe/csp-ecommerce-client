import { Fragment, useEffect, useState } from 'react';
import ProductCardAdmin from '../components/ProductCardAdmin';
import { Container, Row } from 'react-bootstrap';


export default function Products(){
	const [products, setProducts] = useState([]);		


	useEffect(() => {
		fetch('https://fathomless-forest-22794.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {
			console.log(data);
		

// Sets the "products" state to map the data retrieved from the fetch request in several "ProductCard Components"
			setProducts(data.map(product => {
					return (
						<ProductCardAdmin key={product.id} productProp={product} />
					)
				})
			);		
		})
	}, [])


return (

		<Container>
		<Fragment> 
		<h1 className="mt-3 mb-4">Products</h1>
		</Fragment>
		<Row>
		<Fragment> 
			{products}
		</Fragment>
		</Row>
		</Container>
	)
}