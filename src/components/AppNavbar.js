import { Fragment, useContext } from 'react';
// Import necessary components from react-bootstrap
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';
import { Icon } from '@iconify/react';

// AppNavbar component 
export default function AppNavbar(){
	// State to store the user information stored in the login page
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	const {user} = useContext(UserContext);

	return(

		(user.isAdmin === true) ?
			<Navbar bg="dark" variant="dark">
			<Icon icon="mdi:hamburger" height="3em" className="logocolor" />
		    	<Navbar.Brand id="brand" as={NavLink} to="/" exact>BURGLER'S</Navbar.Brand>
		     		<Navbar.Toggle aria-controls="basic-navbar-nav" />
			 			<Navbar.Collapse id="basic-navbar-nav">
			    				<Nav className="ml-auto">

			    					<Nav.Link as={NavLink} to="/admin" exact>Dashboard</Nav.Link>
			    					<Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>

			    				</Nav>
			    	</Navbar.Collapse>
			</Navbar>:		    				


			<Navbar bg="dark" variant="dark">
			<Icon icon="mdi:hamburger" height="3em" className="logocolor" />
		    	<Navbar.Brand id="brand" as={NavLink} to="/" exact>BURGLER'S</Navbar.Brand>
		     		<Navbar.Toggle aria-controls="basic-navbar-nav" />
			 			<Navbar.Collapse id="basic-navbar-nav">
			    				<Nav className="ml-auto">
			      					

			      					{(user.id !== null) ?
			      					  <Fragment>
			      					  <Nav.Link as={NavLink} to="/cart" exact>Cart</Nav.Link>
			      					  <Nav.Link as={NavLink} to="/order-history" exact>My Orders</Nav.Link>
			      					  <Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
			      					  </Fragment>
			      					  :
			      					  <Fragment>
			      					  	<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
			      						<Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
			      					  </Fragment>
			      					}
			      					
			    				</Nav>
			  			</Navbar.Collapse>
		  	</Navbar>
	);
}
