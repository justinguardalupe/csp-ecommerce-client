import { Row, Col, Card, Image } from 'react-bootstrap';
export default function Highlights(){
	return (
		<Row className="mt-2 mb-3">
			<Col xs={6} md={6}>
			<Image src="https://images.unsplash.com/photo-1551782450-a2132b4ba21d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=869&q=80" fluid /> 
				<Card className="cardH bg-light">
				  
				  <Card.Body>
				    <Card.Title>
				    <h4>Classic Burgler</h4>
				    </Card.Title>
				    <Card.Text>
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>

			<Col xs={4} md={3}>
			<Image src="https://images.unsplash.com/photo-1572802419224-296b0aeee0d9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=815&q=80" fluid />
				<Card className="cardH bg-light">
				  
				  <Card.Body>
				    <Card.Title>
				    <h4>Xtra Burgler</h4>
				    </Card.Title>
				    <Card.Text>
				    </Card.Text>
				    	
				  </Card.Body>
				</Card>
			</Col>
			<Col xs={4} md={3}>
			<Image src="https://images.unsplash.com/photo-1565169609627-4b848c72c614?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80" fluid />
				<Card className="cardH bg-light">
				  
				  <Card.Body>
				    <Card.Title>
				    <h4>Healthy Burgler</h4>
				    </Card.Title>
				    <Card.Text>
				    </Card.Text>
				    
				  </Card.Body>
				</Card>
			</Col>
		</Row>
	)
}
