import React from 'react';
import { useState } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import ProductDetails from '../pages/ProductDetails';

export default function CartCard({cartProp}) {
	console.log({cartProp});
	const userId = localStorage.getItem('id');
	const {productId, quantity, price, subtotal, name, description, image} = cartProp;


	return (
		<Card className="m-3">
		  <Card.Body>
		  	
		    <Card.Title></Card.Title>
		    <Card.Text>{productId}</Card.Text>
		    <Card.Text>{name}</Card.Text>
		    <Card.Img className="imageProduct" src={image} fluid/> 
		    <Card.Text>{description}</Card.Text>
		    <Card.Text>₱ {price}</Card.Text>
		    <Card.Text>{quantity}</Card.Text>
		    <Card.Text>₱ {subtotal}</Card.Text>
		    <Link className="btn btn-success" to={`/products`}>CheckOut</Link>
		  </Card.Body>
		</Card>
	)
}


CartCard.propTypes = {
	cart: PropTypes.shape({
		name: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		quantity: PropTypes.number.isRequired,
	}) 
}
