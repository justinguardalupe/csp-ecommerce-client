import PropTypes from 'prop-types';
import { Row, Col, Card, Button, CardGroup, Container } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import React from 'react';
import Swal from 'sweetalert2';


export default function UserCard({userProp}) {
	const {_id, firstName, lastName, email,} = userProp;

		return (
		
			<Col xs={12} md={6} lg={3}  className="mt-2">
						<Card className="p-0 border-1" style={{width: "15rem"}}>
							<Card.Body>
									<div className="d-flex justify-content-center p-0 border-1">
									<Card.Title>User Details</Card.Title>
									</div>
									<Card.Text>User ID: {_id}</Card.Text>
									<Card.Text>Full Name: {firstName} {lastName}</Card.Text>
									<Card.Text>Email: {email}</Card.Text>
								
							</Card.Body>
						</Card>
			</Col>
			)
};
