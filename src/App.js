
import { useState, useEffect, Fragment } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Admin from './pages/Admin';
import Logout from './pages/Logout';
import Cart from './pages/Cart';
import OrderHistory from './pages/OrderHistory';
import AddProduct from './pages/AddProduct';
import ProductDetails from './pages/ProductDetails';
import ProductDatabase from './pages/ProductDatabase';
import AllOrderHistory from './pages/AllOrderHistory';
import CartView from './pages/CartView';
import Users from './pages/Users';
import Error from './pages/Error';
import './App.css';
import { UserProvider } from './UserContext';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  
  return (

        <UserProvider value={{user, setUser, unsetUser}}>
              <Router>
                  <AppNavbar />      
                        <Container>
                            <Switch>
                                  <Route exact path="/" component={Home} />
                                  <Route exact path="/products" component={Products} />
                                  <Route exact path="/register" component={Register} />
                                  <Route exact path="/login" component={Login} />
                                  <Route exact path="/cart" component={Cart} />
                                  <Route exact path="/order-history" component={OrderHistory} />
                                  <Route exact path="/logout" component={Logout} />
                                  <Route exact path="/products/:productId" component={ProductDetails} />
                                  <Route exact path="/products/carts/:userId" component={CartView} />
                                  {(user.isAdmin === true)?
                                  <Fragment>
                                  <Route exact path="/add-product" component={AddProduct} />
                                  <Route exact path="/admin" component={Admin} />
                                  <Route exact path="/user-database" component={Users} />
                                  <Route exact path="/product-database" component={ProductDatabase} />
                                  <Route exact path="/order-database" component={AllOrderHistory} />
                                  </Fragment>
                                  :
                                  <Route component={Error} />
                                }
                            </Switch>
                        </Container>
              </Router>  
        </UserProvider> 
  );
}

export default App;
